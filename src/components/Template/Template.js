import React from "react";

import styled from "styled-components";
import { colors } from "../../assets/styles/GlobalColors";

const Template = styled.div`
  width: 100%;
  border: 1px solid ${colors.Brown};
  display: flex;
  border-radius: 5px;
  box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.9);
  margin-bottom: 20px;
  cursor: pointer;

  .statusIndicator {
    width: 15px;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    background-color: ${({ theme, statusColor }) => theme.colors[statusColor]};
  }
  .orderInfo {
    flex: 1;
    padding: 10px 25px;
    display: grid;
    grid-template-columns: 220px 1fr 1fr 1fr 0.5fr;

    .productIntro {
      display: flex;
      .productImage {
        width: 80px;
        height: 80px;
        border-radius: 10px;
        margin-right: 20px;
        flex: 0.7;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
      }
      .productName {
        font-weight: bold;
        font-size: 18px;
        color: ${colors.Primary};
        flex: 1;
        display: flex;
        align-items: center;
        line-height: 21px;
      }
    }
    .productCategory {
      color: ${colors.Secondary};
      font-weight: bold;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .productSize {
      color: ${colors.Secondary};
      font-weight: bold;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .productColor {
      color: ${colors.Secondary};
      font-weight: bold;
      display: flex;
      align-items: center;
      justify-content: center;
      padding-left: 10px;
    }
    .productInitials {
      color: ${colors.White};
      font-weight: bold;
      display: flex;
      align-items: center;
      justify-content: flex-end;
      .productInitialsWrapper {
        width: 40px;
        height: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
        box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
        border: 1px solid ${colors.Brown};
        border-radius: 4px;
      }
    }
  }
`;

//TODO: Create Json data to filter catagories.
//TODO: Export Data from Json.
export default ({ data }) => {
  const {
    ID,
    Product_name,
    Category,
    Size,
    Colour,
    Status,
    Customers_Initials,
    ImageName,
  } = data;

  return (
    <Template statusColor={Status} order-id={ID}>
      <span className="statusIndicator"></span>
      <div className="orderInfo">
        <div className="productIntro">
          <div
            style={{
              backgroundImage:
                "url(" + require(`../../assets/Images/${ImageName}.jpg`) + ")",
            }}
            className="productImage"
          ></div>
          <div className="productName"> {Product_name} </div>
        </div>
        <div className="productCategory">
          Category: <br />
          {Category}
        </div>
        <div className="productSize">
          Size: <br />
          {Size}
        </div>
        <div className="productColor">
          Colour: <br />
          {Colour}
        </div>
        <div className="productInitials">
          <div className="productInitialsWrapper">{Customers_Initials}</div>
        </div>
      </div>
    </Template>
  );
};
