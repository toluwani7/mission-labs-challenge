import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

/** Create Styled Component for the Project */
const Button = styled.div`
  display: flex;
  align-items: center;
  border-radius: 3px;
  padding: 10px;
  margin: 5px;
  text-transform: uppercase;
  text-decoration: ${({ active }) => (active ? "underline" : "none")};
  font-size: 11px;
  color: #fff;
  font-weight: 500;
  user-select: none;
  cursor: pointer;
  letter-spacing: 2px;
  align-items: center;
  .indicator {
    display: inline-block;
    width: 15px;
    height: 15px;
    border-radius: 50%;
    margin-right: 6px;
    border: 1px solid #fff;
    background-color: ${({ theme, statusColor }) => theme.colors[statusColor]};
  }
`;

Button.propTypes = {};

export default ({ children, onClickHandler, statusColor, active }) => (
  <Button statusColor={statusColor} onClick={onClickHandler} active={active}>
    <span className="indicator"></span>
    {children}
  </Button>
);
