import React from "react";
import { ThemeProvider } from "styled-components";

const Theme = ({ children, variant }) => (
  <ThemeProvider theme={variant}>{children}</ThemeProvider>
);

export default Theme;
