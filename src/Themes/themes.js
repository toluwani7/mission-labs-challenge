/* Learn Styled-Component to create colors for theme*/
export default {
  dark: {
    background: "#222222",
    colors: {
      readyToGo: "#46d7b0",
      onTheWay: "#6dabe3",
      InTheQueue: "#fe6730",
      OutOfStock: "#fc0004",
    },
  },

  //TODO Test color.
  light: {
    background: "rgb(59, 51, 29)",
    colors: {
      readyToGo: "#006600",
      onTheWay: "#800080",
      InTheQueue: "#ffff00",
      OutOfStock: "#ff0000",
    },
  },
};
