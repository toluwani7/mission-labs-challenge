export const colors = {
    readyToGo: "#46d7b0",
    onTheWay:  "#6dabe3",
    InTheQueue: "#fe6730",
    OutOfStock: "#fc0004",
    Primary: "#fff",
    Secondary: "#86878c",
    White: "#fefefe",
    Brown: "#88888a",

};