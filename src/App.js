import React from 'react';
import { Switch, Route } from 'react-router-dom';

import OrderPage from "./Page/OrderPage/"

function App() {
  return (
    <div>
      <Switch>
        <Route exact path='/' component={OrderPage} />
      </Switch>
    </div>
  );
}

export default App;
