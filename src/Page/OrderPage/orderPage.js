import React from "react";
import StatusButton from "../../components/StatusButton";
import styles from "./style.module.scss";
import Template from "../../components/Template";
import data from "../../data.json";

import ThemeProvider from "../../Themes/ThemeProvider";
import themes from "../../Themes/themes";

import Pagination from "react-js-pagination";

import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle` 
 body{ background-color: ${({ theme }) => theme.background};
  top: 0;
  left: 0;
  width: 100%;
  .themePicker {
    color: #fff;
    padding: 50px;
    padding-bottom:0px;
    span {
      cursor: pointer;
      margin-top:10px;
      display:inline-block;
    }
  }
}
`;

export default class OrderPage extends React.Component {
  state = {
    theme: { variant: "dark" },
    activePage: 1,
    itemsCount: 5,
    data: [],
    pageData: [],
    filtered: [],
    selectedFilter: "",
    interval: "",
  };
  constructor() {
    super();
    this.handlePageChange = this.handlePageChange.bind(this);
  }
  componentDidMount() {
    const { activePage, itemsCount } = this.state;

    this.setState(
      {
        totalCount: data.length,
        data,
        filtered: data,
        offset: (activePage - 1) * itemsCount,
      },
      () => {
        const { offset, itemsCount } = this.state;
        const pageData = data.slice(offset, offset + itemsCount);
        this.setState({ pageData });

        this.setIntervalTimer();
      }
    );
  }

  setIntervalTimer() {
    const { interval: previousInterval } = this.state;
    if (previousInterval) {
      clearInterval(previousInterval);
    }
    const interval = setInterval(() => {
      console.log("running");
      const { activePage, itemsCount, filtered } = this.state;
      const newPage =
        Math.ceil(filtered.length / itemsCount) <= activePage
          ? 1
          : activePage + 1;
      this.handlePageChange(newPage);

      console.log(filtered.length / itemsCount);
    }, 10000);

    this.setState({ interval });
  }

  handlePageChange(pageNumber) {
    const { itemsCount, filtered } = this.state;
    this.setState(
      { activePage: pageNumber, offset: (pageNumber - 1) * itemsCount },
      () => {
        const { offset, itemsCount } = this.state;
        const pageData = filtered.slice(offset, offset + itemsCount);
        this.setState({ pageData });
      }
    );
  }

  changeTheme(variant) {
    const theme = { ...this.state.theme, variant: variant };
    this.setState({ theme });
  }
  handleStatusChange(statusID) {
    const { data: oldData } = this.state;
    const filtered =
      statusID === null
        ? oldData
        : oldData.filter((piece) => {
            const { Status } = piece;

            return Status === statusID;
          });

    this.setState({ filtered, activePage: 1, offset: 0 }, () => {
      const { offset, itemsCount, filtered } = this.state;
      const pageData = filtered.slice(offset, offset + itemsCount);
      this.setState(
        {
          pageData,
          totalCount: filtered.length,
          selectedFilter: statusID,
        },
        () => {
          this.setIntervalTimer();
        }
      );
    });
  }

  render() {
    const {
      theme,
      activePage,
      itemsCount,
      totalCount,
      pageData,
      selectedFilter,
    } = this.state;

    return (
      <ThemeProvider variant={themes[theme.variant]}>
        <GlobalStyle />
        <div className="themePicker">
          <div>Click to change theme:</div>
          <span
            onClick={() => {
              this.changeTheme("dark");
            }}
          >
            Dark Theme
          </span>{" "}
          |{" "}
          <span
            onClick={() => {
              this.changeTheme("light");
            }}
          >
            Colour Theme
          </span>{" "}
        </div>
        <div className={styles.wrapper}>
          <div className={styles.statusWrapper}>
            {selectedFilter && (
              <span
                onClick={() => {
                  this.handleStatusChange(null);
                }}
                className={styles.resetFilters}
              >
                All
              </span>
            )}
            <StatusButton
              onClickHandler={() => {
                this.handleStatusChange("readyToGo");
              }}
              statusColor={"readyToGo"}
              active={selectedFilter === "readyToGo"}
            >
              Ready to go
            </StatusButton>
            <StatusButton
              onClickHandler={() => {
                this.handleStatusChange("onTheWay");
              }}
              statusColor={"onTheWay"}
              active={selectedFilter === "onTheWay"}
            >
              On the Way
            </StatusButton>
            <StatusButton
              onClickHandler={() => {
                this.handleStatusChange("InTheQueue");
              }}
              statusColor={"InTheQueue"}
              active={selectedFilter === "InTheQueue"}
            >
              In The Queue
            </StatusButton>
            <StatusButton
              onClickHandler={() => {
                this.handleStatusChange("OutOfStock");
              }}
              statusColor={"OutOfStock"}
              active={selectedFilter === "OutOfStock"}
            >
              Out of Stock
            </StatusButton>
          </div>

          {pageData.map((piece) => (
            <Template data={piece} />
          ))}
          <div className={styles.footerWrapper}>
            <div></div>
            <div>
              <Pagination
                activePage={activePage}
                itemsCountPerPage={itemsCount}
                totalItemsCount={totalCount}
                pageRangeDisplayed={5}
                hideFirstLastPages={true}
                onChange={this.handlePageChange}
                innerClass={styles.pagination}
                itemClass={styles.paginateItem}
                itemClassPrev={styles.hidden}
                itemClassNext={styles.hidden}
                activeClass={styles.active}
              />
            </div>
            <div className={styles.pageNumber}>
              0{activePage} | 0{Math.ceil(totalCount / itemsCount)}
            </div>
          </div>
        </div>
      </ThemeProvider>
    );
  }
}
